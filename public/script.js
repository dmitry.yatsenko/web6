let x = 10000;
function onClick(){
let e1 = parseInt(document.getElementById("entryfield1").value);
if(Number.isInteger(e1) && Number.isInteger(x)) {
document.getElementById("printfield").innerHTML = e1*x;}
else
 {document.getElementById("printfield").innerHTML = ""; }
}
window.addEventListener("DOMContentLoaded", function (event) {
console.log("DOM fully loaded and parsed");

	let b = document.getElementById("button1");
	let value = -1; 
	b.addEventListener("click", onClick);
	let sel=document.getElementsByName("selection");
	let rbuttons = document.getElementById("radios");
	let mycheckbox = document.getElementById("mycheckbox");
	rbuttons.style.display = "none";
	mycheckbox.style.display = "none";
	let a = "Rouble";
	sel[0].addEventListener("change",function(event){
		let select = event.target;
		if (select.value === "1")
		{
			x = 10000;
			rbuttons.style.display = "none";
			mycheckbox.style.display = "none";
			onClick();
		} else if(select.value === "2")
		{
			x = 5000;
			rbuttons.style.display = "flex";
			rbuttons.style.flexDirection = "row";
			mycheckbox.style.display = "none";
			onClick();
		}else {
			x = 1000;
			mycheckbox.style.display = "flex";
			mycheckbox.style.flexDirection = "row";
			rbuttons.style.display = "none";
			onClick();
		}
	});
	rbuttons.addEventListener("click", function(event){
			let v1 = parseInt(document.getElementById("entryfield1").value);
			let r = event.target;
			if(Number.isInteger(v1) && Number.isInteger(x)) {
				if(r.value === "Euro")
				{	value = Math.floor((v1*x)/89);
					document.getElementById("printfield").innerHTML = value;
				}else if(r.value === "Dollar") {
					value = Math.floor((v1*x)/76);
					document.getElementById("printfield").innerHTML = value;
				} else {
					value = Math.floor(v1*x);
					document.getElementById("printfield").innerHTML = value;
				}
			}else {
				document.getElementById("printfield").innerHTML = "";}
		});
	mycheckbox.addEventListener("click", function(event){
		let e = parseInt(document.getElementById("entryfield1").value);
		let k = event.target;
		if(k.checked){
			x = 2000;
		} else {
                 x = 1000;
              }
              document.getElementById("printfield").innerHTML = e * x;
	});
});
